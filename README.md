# GTK+ 3 Tutorial

## Installation

This assumes you have `gcc` and `make`.

- libgtk-3-dev
- libgtk-3-doc
- libwebkit2gtk-4.0-dev
- libwebkit2gtk-4.0-doc

### apt

```
sudo apt install libgtk-3-dev libgtk-3-doc libwebkit2gtk-4.0-dev libwebkit2gtk-4.0-doc
```
