# Executable file name
TARGET = app

CC = gcc
SRC_DIR = src
OBJ_DIR = obj
DEBUG = -g
OPTIMIZE = -O0
WARNINGS = -Wall
PTHREAD = -pthread
NO_LINK = -c
USE_PIPE = -pipe
CCFLAGS = $(DEBUG) $(OPTIMIZE) $(WARNINGS) $(PTHREAD) $(NO_LINK) $(USE_PIPE)
GTKLIB = `pkg-config --cflags --libs gtk+-3.0 webkit2gtk-4.0`
OBJS = main.o

# Linker
LD = gcc
LDFLAGS = $(PTHREAD) $(GTKLIB) -export-dynamic

SRCS = $(SRC_DIR)/main.c
OBJS = $(OBJ_DIR)/main.o

all: clean install

install: $(OBJS)
	$(LD) -o $(TARGET) $(OBJS) $(LDFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	mkdir obj
	$(CC) -c $(CCFLAGS) $< $(GTKLIB) -MD -o $@

run:
	./$(TARGET)

clean:
	rm -rf $(OBJ_DIR) $(TARGET)
